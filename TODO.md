

# TODO

1. Create web server
2. Setup Sqlite as a database
3. Create way to get the information from AoC
   1. Use the leaderboard API?
4. Output said data in a format that https://shields.io/ can use?


# Create web server
Ive got this mostly done for anoyther project, copy it in

Pop under https://api.brendan.ie/aoc/?

# Setup Sqlite as a database
This cant be too hard, right?

It has to function as a cache, owner of AoC does not like too many requests:

> avoid sending requests more often than once every 15 minutes (900 seconds).

User info Table

* User ID
* Leaderboard ID
* Session key
  * Required for getting teh leaderboard results

Results Table

* User ID
* Year
* Stars
* Last Request


# Create way to get the information from AoC
``https://adventofcode.com/<year>/leaderboard/private/view/<leaderboard id>.json``

```json
{
  "event": "2021",
  "owner_id": "1043391",
  "members": {
    "1043391": {
      "stars": 8,
      "id": "1043391",
      "completion_day_level": {},
      "local_score": 59,
      "global_score": 0,
      "last_star_ts": 1638633814,
      "name": "Brendan Golden"
    }
  }
}
```

# Format for https://shields.io/
Docs on https://shields.io/endpoint


```json
{
  "schemaVersion": 1,
  "label": "hello",
  "message": "sweet world",
  "color": "orange"
}
```