{
	description = "Display AoC Stars on Readme badges";
  
  inputs = {
    utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";
  };

  outputs = { self, nixpkgs, utils, naersk }: utils.lib.eachDefaultSystem (system: 
    let
      pkgs = nixpkgs.legacyPackages."${system}";
      naersk-lib = naersk.lib."${system}";
      package_name = "aoc-badge-api";
    in rec {

      # `nix build`
      packages."${package_name}" = naersk-lib.buildPackage {
        pname = "${package_name}";
        root = ./.;
        
        buildInputs = [
          pkgs.openssl
          pkgs.pkg-config
        ];
      };

      defaultPackage = packages."${package_name}";

      # `nix run`
      apps."${package_name}" = utils.lib.mkApp {
        drv = packages."${package_name}";
      };

      defaultApp = apps."${package_name}";

      # `nix develop`
      devShell = pkgs.mkShell {
        nativeBuildInputs = with pkgs; [ rustc cargo ];
      };
      
      nixosModule = { lib, pkgs, config, ... }: 
        with lib; 
        let
          cfg = config.services."${package_name}";
        in { 
          options.services."${package_name}" = {
            enable = mkEnableOption "enable ${package_name}";
            
            database = mkOption rec {
              type = types.str;
              default = "database.db";
              example = default;
              description = "The path to the database";
            };
            
            host_port = mkOption rec {
              type = types.str ;
              default = "8060";
              example = default;
              description = "port for the program";
            };
            
            
           # specific for teh program running
           user = mkOption rec {
              type = types.str;
              default = "${package_name}";
              example = default;
              description = "The user to run the service";
           };
           
           home = mkOption rec {
              type = types.str;
              default = "/etc/silver_${package_name}";
              example = default;
              description = "The home for the user";
           };
            
          };

          config = mkIf cfg.enable {
          
            users.groups."${cfg.user}" = { };
            
            users.users."${cfg.user}" = {
              createHome = true;
              isSystemUser = true;
              home = "${cfg.home}";
              group = "${cfg.user}";
            };
            
            systemd.services."${cfg.user}" = {
              description = "Display AoC Stars";
              
              wantedBy = [ "multi-user.target" ];
              after = [ "network-online.target" ];
              wants = [ ];
              serviceConfig = {
                # fill figure this out in teh future
                #DynamicUser=true;
                User = "${cfg.user}";
                Group = "${cfg.user}";
                Restart = "always";
                ExecStart = "${self.defaultPackage."${system}"}/bin/aoc_badge_api ${cfg.database} ${cfg.host_port}";
              };
            };
            
          };
          
        };
      
    });
}