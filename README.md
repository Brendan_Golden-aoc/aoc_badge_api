# AoC Badge API

This program grabs the stars you have gotten via the leaderboard API and returns a shield.io parseable result.

Like so:  
![Stars Total](https://img.shields.io/endpoint?url=https://api.brendan.ie/aoc/get/1043391/all) ![Stars 2021](https://img.shields.io/endpoint?url=https://api.brendan.ie/aoc/get/1043391/2021) ![Stars 2022](https://img.shields.io/endpoint?url=https://api.brendan.ie/aoc/get/1043391/2022)


# How to use.
1. Create your own leaderboard on https://adventofcode.com/2021/leaderboard/private.
2. Sign into AoC ona  new browser and grab teh session key from it
   * https://github.com/gobanos/cargo-aoc#setting-up-the-cli has instructions on how to do it.

3. Send a request to the api with the ID and session as parameters
   * id is the ID of the private leaderboard, which also happens to be your AoC ID
   * ``https://api.brendan.ie/aoc/add?id=<id>&session=<session>``
4. Once the details are set you can call a response
   *``` https://api.brendan.ie/aoc/get/<id>/<year>```
   * Year can be any numeric year like 2021 or it can be all
     * ``https://api.brendan.ie/aoc/get/<id>/2021``
     * ``https://api.brendan.ie/aoc/get/<id>/all``
5. Use that link at https://shields.io/endpoint as teh basis for your badge

# How to complile for yerself
1. Have rust 2021
2. Cargo build?
3. ...
4. rest of teh owl.


# Limitations
* It will only get *your* data from *your* personal leaderboard.
* Yearly data is only updated on request and has a cache time of at least an hour before fresh requests are made to AoC again.


# Backstory
A few days ago I saw [Will Bonnell]'s post about creatng badges for git repos, I loved the idea of doing that.  
However, he used a web scraper to get the info which didnt sit so well with me.  
But then I was checking out teh leaderboard I set up to track the progress of my classmates I noticed that it had a json option.

So with that in hand I decided to spend an evening trying to put together a solution, with the goal of improving at rust.  
Being the idiot that I am I [streamed it][video], all 8 hours...

That being said it works!  
And I got some better experience in managing a webserver in rust, for my JS/TS stuff I am spoilt by PM2.  
This project got me to look into creating a service for it, it was pretty painless to do :3.

Of course when I was finished I googled "AoC Badge" and [two][site one] [existing][site two] projects popped up, although one was github specific.  
Have I already mentioned that I can be an idiot?


[Will Bonnell]: https://willdebras.github.io/posts/advent/
[video]: https://youtu.be/8nKhTzeWsgI
[site one]: https://github.com/stackcats/adventofcode-badge
[site two]: https://github.com/J0B10/aoc-badges-action
  
