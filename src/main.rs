use axum::extract::{Path, Query};
use axum::http::StatusCode;
use axum::response::{IntoResponse, Redirect, Response};
use axum::routing::get;
use axum::{Extension, Json, Router};
use reqwest::header::{ACCEPT, CONTENT_TYPE, USER_AGENT};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use sqlx::sqlite::{SqliteConnectOptions, SqlitePoolOptions};
use sqlx::{Error, Pool, Sqlite};
use std::collections::HashMap;
use std::env;
use std::net::SocketAddr;
use std::str::FromStr;
use std::time::{SystemTime, UNIX_EPOCH};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let args: Vec<String> = env::args().collect();
    let database = if args.len() > 1 { &args[1] } else { "database.db" };
    let port = if args.len() > 2 { args[2].parse::<u16>().unwrap_or(8060) } else { 8060 };

    let pool = db_init(database).await?;

    let addr = SocketAddr::from(([127, 0, 0, 1], port));
    let app = Router::new()
        // add a wildcard rediect top take care of non mathcing paths
        .route("/aoc", get(|| async { Redirect::permanent("https://gitlab.com/Brendan_Golden-aoc/aoc_badge_api") }))
        .route("/aoc/", get(|| async { Redirect::permanent("https://gitlab.com/Brendan_Golden-aoc/aoc_badge_api") }))
        // one endpint to set the userID, leaderboard ID and session key
        .route("/aoc/add", get(user_data_add))
        // one endpoint to remove the userID, leaderboard ID and session key
        .route("/aoc/delete", get(user_data_delete))
        // one endpoint tog et the data
        // can request a single year or all of them
        .route("/aoc/get/:user/:year", get(user_results_get))
        .layer(Extension(pool));

    axum::Server::bind(&addr).serve(app.into_make_service()).await?;

    Ok(())
}

async fn db_init(database: &str) -> Result<Pool<Sqlite>, Error> {
    let pool = SqlitePoolOptions::new().max_connections(5).connect_with(SqliteConnectOptions::from_str(&format!("sqlite://{}", database))?.create_if_missing(true)).await?;

    sqlx::query(
        "CREATE TABLE IF NOT EXISTS user_data (
            id text primary key,
            session text not null
         )",
    )
    .execute(&pool)
    .await?;

    sqlx::query(
        "CREATE TABLE IF NOT EXISTS user_results (
            id text not null,
            year integer not null,
            stars integer not null,
            last_request integer not null,
            PRIMARY KEY (id, year)
         )",
    )
    .execute(&pool)
    .await?;

    // set up indexes?

    Ok(pool)
}

#[derive(Default, Debug, Deserialize, sqlx::FromRow)]
#[serde(default)]
struct UserData {
    id: String,
    session: String,
}

async fn user_data_add(Query(params): Query<HashMap<String, String>>, Extension(pool): Extension<Pool<Sqlite>>) -> Response {
    let id = if let Some(x) = params.get("id") { x } else { return (StatusCode::BAD_REQUEST, "No id").into_response() };
    let session = if let Some(x) = params.get("session") { x } else { return (StatusCode::BAD_REQUEST, "No session").into_response() };

    match sqlx::query_as::<_, UserData>("INSERT OR REPLACE INTO user_data (id, session) VALUES (?1, ?2)").bind(id).bind(session).fetch_optional(&pool).await {
        Ok(_) => (StatusCode::OK, format!("Successfully added user: {}", id)).into_response(),
        Err(_) => (StatusCode::OK, format!("Error adding user: {}", id)).into_response(),
    }
}

async fn user_data_delete(Query(params): Query<HashMap<String, String>>, Extension(pool): Extension<Pool<Sqlite>>) -> Response {
    let id = if let Some(x) = params.get("id") { x } else { return (StatusCode::BAD_REQUEST, "No id").into_response() };

    sqlx::query_as::<_, UserData>("DELETE FROM user_data WHERE id = ?").bind(id).fetch_optional(&pool).await.unwrap_or_default();

    /* keep this data in case some jackass decides to  create, request and delete repeatedly to get me to try and ddos AoC
    sqlx::query_as::<_, UserResults>("DELETE FROM user_results WHERE id = ?")
        .bind(&entry.id)
        .fetch_optional(pool)
        .await?;
     */

    (StatusCode::OK, format!("Successfully deleted user: {}", id)).into_response()
}

async fn user_results_get(Path(params): Path<HashMap<String, String>>, Extension(pool): Extension<Pool<Sqlite>>) -> Json<Value> {
    let user = if let Some(x) = params.get("user") { x } else { return Json(json!({"schemaVersion": 1,"label": "Error","message": "User Unknown","color": "red"})) };
    let year = if let Some(x) = params.get("year") { x } else { return Json(json!({"schemaVersion": 1,"label": "Error","message": "User Unknown","color": "red"})) };

    // get the account details
    let user_account = sqlx::query_as::<_, UserData>("SELECT * FROM user_data WHERE id = ?").bind(user).fetch_all(&pool).await.unwrap_or_default();

    if user_account.is_empty() {
        return Json(json!({"schemaVersion": 1,"label": "Error","message": "User Unknown","color": "red"}));
    }

    // here it is known that the user exists
    // now we need to pull the year

    let mut stars = 0;
    let mut label = format!("Stars {}", &year);
    if year == "all" {
        label = String::from("Stars Total");
        let mut year_value = 2015;
        while let Some(x) = get_year(&pool, &user_account[0], year_value).await {
            stars += x;
            year_value += 1;
        }
    } else if let Ok(year_value) = year.parse::<i32>() {
        if let Some(x) = get_year(&pool, &user_account[0], year_value).await {
            stars += x;
        }
    }
    Json(json!({"schemaVersion": 1,"label": label,"message": stars.to_string(),"color": "green"}))
}

#[derive(Default, Debug, Deserialize, sqlx::FromRow)]
#[serde(default)]
struct UserResults {
    id: String,
    year: u32,
    stars: u32,
    last_request: i64, // seems sqlite does not support u64
}

async fn get_year(pool: &Pool<Sqlite>, user: &UserData, year: i32) -> Option<u32> {
    /*
    get the data for that user for that year,
    if ti does not exist
        get it
        save it
    else
        if more than an hour has passed
            get it
            save it
        else
            return the archived version

    return value
    */
    let result_db = sqlx::query_as::<_, UserResults>("SELECT * FROM user_results WHERE id = ? AND year = ?").bind(&user.id).bind(year).fetch_all(pool).await.ok()?;

    let current_time = if let Ok(x) = SystemTime::now().duration_since(UNIX_EPOCH) { x.as_secs() as i64 } else { 0 };

    // 3600s is an hour
    if !result_db.is_empty() && (current_time - result_db[0].last_request) < 3600 {
        // return cached value
        Some(result_db[0].stars)
    } else if let Some(stars) = get_year_getter(user, year).await {
        // get a new value

        // save it
        sqlx::query_as::<_, UserResults>("INSERT OR REPLACE INTO user_results (id, year, stars, last_request) VALUES (?1, ?2, ?3, ?4)")
            // formatting
            .bind(&user.id)
            .bind(year)
            .bind(stars)
            .bind(current_time)
            .fetch_optional(pool)
            .await
            .ok()?;
        // return value
        Some(stars)
    } else if !result_db.is_empty() {
        // getting a new number failed
        Some(result_db[0].stars)
    } else {
        // no previous values
        None
    }
}

/*
#[derive(Serialize, Deserialize, Debug)]
struct LeaderboardResponseInnerInner {
    get_star_ts: u64,
    star_index: u32,
}
*/

#[derive(Serialize, Deserialize, Debug)]
struct LeaderboardResponseInner {
    id: u32,
    //local_score: u32,
    //last_star_ts: u64,
    //completion_day_level: HashMap<u32, HashMap<u32, LeaderboardResponseInnerInner>>,
    stars: u32,
    //global_score: u32,
    //name: String,
}
#[derive(Serialize, Deserialize, Debug)]
struct LeaderboardResponse {
    owner_id: u32,
    event: String,
    members: HashMap<String, LeaderboardResponseInner>,
}

async fn get_year_getter(user: &UserData, year: i32) -> Option<u32> {
    let client = reqwest::Client::new();

    let request = client
        .get(format!("https://adventofcode.com/{}/leaderboard/private/view/{}.json", year, &user.id))
        // readability
        .header("cookie", format!("session={}", &user.session))
        .header(CONTENT_TYPE, "application/json")
        .header(ACCEPT, "application/json")
        .header(USER_AGENT, "gitlab.com/Brendan_Golden-aoc/aoc_badge_api by aoc_badge@brendan.ie")
        .send()
        .await;

    if let Ok(x) = request {
        match x.json::<LeaderboardResponse>().await {
            Ok(parsed) => {
                for (_, val) in parsed.members.iter() {
                    if parsed.owner_id == val.id {
                        return Some(val.stars);
                    }
                }
            }
            Err(e) => println!("{}", e),
        };
    }

    None
}
